/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const config = require('../config/easy-log-watcher')
const logWatcher = require('../easy-log-watcher')
const path = require('path')

/* Test prerequisites */
const Code = require('code')
const Lab = require('lab')
const lab = (exports.lab = Lab.script())
const describe = lab.describe
const it = lab.it
const expect = Code.expect

describe('no file', { timeout: 10 * 1000 }, function () {
  it('loop stopped', function (fin) {
    /* Fires the test */
    logWatcher.start(getAnalyzers(), getOptions())
    .then(function (result) {
      /* Checks the result */
      expect(result.success).to.equal(true)
      fin()
    })
    /* Waits some delay before stops the log watcher */
    setTimeout(function () {
      logWatcher.stop()
    }, 1000)
  })
})

/* ---------- FUNCTIONS ---------- */

function getAnalyzers () {
  return [{
    filename: path.join(__dirname, 'nofile.log'),
    parsers: [{
      checker: function (line, values) { return line.indexOf('error') > -1 },
      trigger: function (file, line, values) { return 0 }
    }]
  }]
}

function getOptions () {
  return {
    delay: 0.5,
    logger: null,
    sequential: false
  }
}
