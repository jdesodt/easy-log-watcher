/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const config = require('../config/easy-log-watcher')
const logWatcher = require('../easy-log-watcher')
const path = require('path')

/* Test prerequisites */
const Code = require('code')
const Lab = require('lab')
const lab = (exports.lab = Lab.script())
const describe = lab.describe
const it = lab.it
const expect = Code.expect

describe('start and stop', { timeout: 8 * 1000 }, function () {
  /* Runs in background */
  it('in background', function (fin) {
    /* Initializes */
    logWatcher.count = 0
    /* Fires the test */
    logWatcher.start(getAnalyzersLoopIndefinitely(logWatcher), getOptions())
    .then(function (result) {
      /* Checks the result */
      expect(result.success).to.equal(true)
      expect(logWatcher.count).to.equal(2)
      fin()
    })
    /* Waits some delay before stops the log watcher */
    setTimeout(function () {
      logWatcher.stop()
    }, 1500)
  })
  /* Stops in the parser */
  it('with the parser', function (fin) {
    /* Fires the test */
    logWatcher.start(getAnalyzersWithStop(), getOptions())
    .then(function (result) {
      /* Checks the result */
      expect(result.success).to.equal(true)
      fin()
    })
  })
})

/* ---------- FUNCTIONS ---------- */

function getAnalyzersLoopIndefinitely (logWatcher) {
  return [{
    filename: path.join(__dirname, 'watcher-log.txt'),
    parsers: [{
      checker: function (line, values) { return line.indexOf('error') > -1 },
      trigger: function (file, line, values) {
        values.logWatcher.count ++
        return 0 // Loops indefinitely
      },
      values: { logWatcher: logWatcher }
    }]
  }]
}

function getAnalyzersWithStop () {
  return [{
    filename: path.join(__dirname, 'watcher-log.txt'),
    parsers: [{
      checker: function (line, values) { return line.indexOf('error') > -1 },
      trigger: function (file, line, values) { return 5 } // stops the log watcher
    }]
  }]
}

function getOptions () {
  return {
    delay: 0.5,
    logger: null,
    sequential: false
  }
}
