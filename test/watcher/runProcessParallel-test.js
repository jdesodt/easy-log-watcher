/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const config = require('../../config/easy-log-watcher')
const path = require('path')
const processAnalyzer = require('../../process/analyzer')
const processWatcher = require('../../process/watcher')

/* Test prerequisites */
const Code = require('code')
const Lab = require('lab')
const lab = (exports.lab = Lab.script())
const after = lab.after
const describe = lab.describe
const it = lab.it
const expect = Code.expect

/* Backups functions before mock */
const backups = new Map()
backups.set(processAnalyzer, [
  ['run', processAnalyzer.run.bind({})]
])

describe('watcher runProcessParallel', { timeout: 5 * 1000 }, function () {
  after((done) => {
    /* Restores the origin functions */
    for (var [key, values] of backups) {
      values.forEach(function (item) { key[item[0]] = item[1] })
    }
    done()
  })
  /* Error */
  it('error', function (fin) {
    /* Mocks */
    var msg = 'Ooops!'
    mockAnalyzerError(msg)
    /* Fires the test */
    processWatcher.runProcessParallel(getAnalyzers(), getOptionsParallel())
    .catch(function (err) {
      /* Checks the result */
      expect(err.message).to.equal(msg)
      fin()
    })
  })
  it('run', function (fin) {
    /* Initializes */
    processWatcher.count = 0
    /* Mocks */
    mockAnalyzerRun(processWatcher)
    /* Fires the test */
    processWatcher.runProcessParallel(getAnalyzers(), getOptionsParallel())
    .then(function () {
      /* Checks the result */
      expect(processWatcher.count).to.equal(1)
      fin()
    })
  })
})

/* ---------- MOCKS ---------- */

function mockAnalyzerError (msg) {
  processAnalyzer.run = function (processWatcher, analyzer, options) {
    return new Promise(function (resolve, reject) {
      return reject(new Error(msg))
    })
  }
  return false
}

function mockAnalyzerRun (processWatcher) {
  processAnalyzer.run = function (processWatcher, analyzer, options) {
    return new Promise(function (resolve, reject) {
      processWatcher.count ++
      return resolve()
    })
  }
  return false
}

/* ---------- FUNCTIONS ---------- */

function getAnalyzers () {
  return [{
    filename: path.join(__dirname, 'execute-log.txt'),
    parsers: [{
      checker: function (line, values) { return false },
      trigger: function (file, line, values) { return 0 } // Loops indefinitely
    }]
  }]
}

function getOptionsParallel () {
  return {
    delay: 1,
    logger: null,
    sequential: false
  }
}
