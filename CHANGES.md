## 1.2.2 (2018-07-24)
- Reduced the line length in the log message to avoid too big logs

## 1.2.1 (2018-07-19)
- Removed the parser object from the log message to avoid too big logs

## 1.2.0 (2018-07-11)
- Updated process steps for parallel and sequential mode execution
- Updated the tests
- Updated the README file

## 1.1.1 (2018-07-10)
- Fixed typos in the README file
- Updated the Licence Copyright

## 1.1.0 (2018-07-09)
- New analyzer process

## 1.0.0 (2018-07-01)
- First commit
