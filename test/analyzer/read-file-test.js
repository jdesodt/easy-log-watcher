/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const config = require('../../config/easy-log-watcher')
const path = require('path')
const processAnalyzer = require('../../process/analyzer')
const processParser = require('../../process/parser')
const processWatcher = require('../../process/watcher')

/* Test prerequisites */
const Code = require('code')
const Lab = require('lab')
const lab = (exports.lab = Lab.script())
const after = lab.after
const describe = lab.describe
const it = lab.it
const expect = Code.expect

/* Backups functions before mock */
const backups = new Map()
backups.set(processParser, [
  ['runParallel', processParser.runParallel.bind({})],
  ['runSequential', processParser.runSequential.bind({})]
])

describe('analyzer readFile', { timeout: 5 * 1000 }, function () {
  after((done) => {
    /* Restores the origin functions */
    for (var [key, values] of backups) {
      values.forEach(function (item) { key[item[0]] = item[1] })
    }
    done()
  })
  /* No file */
  it('no file', function (fin) {
    /* Fires the test */
    processAnalyzer.readFile(processWatcher, getAnalyzer(), path.join(__dirname, 'nofile.log'), getOptions())
    .then(function () {
      /* Checks the result */
      fin()
    })
  })
  it('no such file', function (fin) {
    /* Mocks */
    mockParallelError(new Error('no such file or directory'))
    /* Fires the test */
    processAnalyzer.readFile(processWatcher, getAnalyzer(), path.join(__dirname, 'analyzer-log.log'), getOptions())
    .then(function () {
      /* Checks the result */
      fin()
    })
  })
  /* Log watcher stop */
  it('log watcher stop', function (fin) {
    /* Mocks */
    mockParallelRun(processWatcher)
    /* Fires the test */
    processAnalyzer.readFile(processWatcher, getAnalyzerParallel(), path.join(__dirname, 'big-log.txt'), getOptions())
    .catch(function (err) {
      /* Checks the result */
      expect(err).to.equal(processAnalyzer.triggerStopWatcher)
      processWatcher.stop = false
      fin()
    })
    /* Waits some delay before stops the log watcher */
    setTimeout(function () {
      processWatcher.stop = true
    }, 400)
  })
  /* File exists, parallel read */
  it('parallel error', function (fin) {
    /* Initializes */
    var message = 'Ooops!'
    /* Mocks */
    mockParallelError(message)
    /* Fires the test */
    processAnalyzer.readFile(processWatcher, getAnalyzerParallel(), path.join(__dirname, 'analyzer-log.txt'), getOptions())
    .catch(function (err) {
      /* Checks the result */
      expect(err.message).to.equal(message)
      fin()
    })
  })
  it('parallel full read', function (fin) {
    /* Mocks */
    mockParallelRun(processWatcher)
    /* Fires the test */
    processAnalyzer.readFile(processWatcher, getAnalyzerParallel(), path.join(__dirname, 'analyzer-log.txt'), getOptions())
    .then(function () {
      /* Checks the result */
      expect(processWatcher.count).to.equal(3)
      fin()
    })
  })
  /* File exists, sequential read */
  it('sequential error', function (fin) {
    /* Initializes */
    var message = 'Ooops!'
    /* Mocks */
    mockSequentialError(message)
    /* Fires the test */
    processAnalyzer.readFile(processWatcher, getAnalyzerSequential(), path.join(__dirname, 'analyzer-log.txt'), getOptions())
    .catch(function (err) {
      /* Checks the result */
      expect(err.message).to.equal(message)
      fin()
    })
  })
  it('sequential full read', function (fin) {
    /* Mocks */
    mockSequentialRun(processWatcher)
    /* Fires the test */
    processAnalyzer.readFile(processWatcher, getAnalyzerSequential(), path.join(__dirname, 'analyzer-log.txt'), getOptions())
    .then(function () {
      /* Checks the result */
      expect(processWatcher.count).to.equal(3)
      fin()
    })
  })
})

/* ---------- MOCKS ---------- */

function mockParallelError (msg) {
  processParser.runParallel = function (processWatcher, processAnalyzer, parsers, file, line, options) {
    return new Promise(function (resolve, reject) {
      return reject(new Error(msg))
    })
  }
  return false
}

function mockParallelParserError (processWatcher, err) {
  processParser.runParallel = function (processWatcher, processAnalyzer, parsers, file, line, options) {
    return new Promise(function (resolve, reject) {
      processWatcher.count ++
      return reject(err)
    })
  }
  return false
}

function mockParallelRun (processWatcher) {
  processWatcher.count = 0
  processParser.runParallel = function (processWatcher, processAnalyzer, parsers, file, line, options) {
    return new Promise(function (resolve, reject) {
      processWatcher.count ++
      return resolve()
    })
  }
  return false
}

function mockSequentialError (msg) {
  processParser.runSequential = function (processWatcher, processAnalyzer, parsers, file, line, options) {
    return new Promise(function (resolve, reject) {
      return reject(new Error(msg))
    })
  }
  return false
}

function mockSequentialParserError (processWatcher, err) {
  processParser.runSequential = function (processWatcher, processAnalyzer, parsers, file, line, options) {
    return new Promise(function (resolve, reject) {
      processWatcher.count ++
      return reject(err)
    })
  }
  return false
}

function mockSequentialRun (processWatcher) {
  processWatcher.count = 0
  processParser.runSequential = function (processWatcher, processAnalyzer, parsers, file, line, options) {
    return new Promise(function (resolve, reject) {
      processWatcher.count ++
      return resolve()
    })
  }
  return false
}

/* ---------- FUNCTIONS ---------- */

function getAnalyzer () {
  return {
    filename: path.join(__dirname, 'analyzer-log.txt'),
    parsers: [{
      checker: function (line, values) { return false },
      trigger: function (file, line, values) { return 0 } // loops indefinitely
    }],
    options: {
      sequentialFiles: false,
      sequentialParsers: false
    }
  }
}

function getAnalyzerParallel () {
  var analyzer = getAnalyzer()
  analyzer.options.sequentialParsers = false
  return analyzer
}

function getAnalyzerSequential () {
  var analyzer = getAnalyzer()
  analyzer.options.sequentialParsers = true
  return analyzer
}

function getOptions () {
  return {
    delay: 1,
    logger: null,
    sequential: false
  }
}
