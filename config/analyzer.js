/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* default configuration file */
var config = {}

/* Read files mode */
config.sequentialFiles = false
/* Run parsers mode */
config.sequentialParsers = false

/* Exports this configuration */
module.exports = config
