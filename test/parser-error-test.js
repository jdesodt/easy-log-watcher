/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const config = require('../config/easy-log-watcher')
const logWatcher = require('../easy-log-watcher')
const path = require('path')

/* Test prerequisites */
const Code = require('code')
const Lab = require('lab')
const lab = (exports.lab = Lab.script())
const describe = lab.describe
const it = lab.it
const expect = Code.expect

describe('parser error', { timeout: 10 * 1000 }, function () {
  it('fired', function (fin) {
    /* Initializes */
    var message = 'Ooops!'
    /* Fires the test */
    logWatcher.start(getAnalyzers(message), getOptions())
    .catch(function (err) {
      /* Checks the result */
      expect(err.message).to.equal(message)
      fin()
    })
  })
})

/* ---------- FUNCTIONS ---------- */

function getAnalyzers (message) {
  return [{
    filename: path.join(__dirname, 'watcher-log.txt'),
    parsers: [{
      checker: function (line, values) { return line.indexOf('error') > -1 },
      trigger: function (file, line, values) { throw new Error(message) }
    }]
  }]
}

function getOptions () {
  return {
    delay: 0.5,
    logger: null,
    sequential: false
  }
}
