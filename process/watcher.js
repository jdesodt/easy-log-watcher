/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const processAnalyzer = require('./analyzer')
const promise = require('bluebird')

/* Initializes */
var processWatcher = {}

/* Executes the looping process */
processWatcher.execute = function (analyzers, options) {
  return new Promise(function (resolve, reject) {
    /* Initializes */
    processWatcher.stop = false
    var loopStop = false
    /* Defines the looping function */
    function loop (loopStop) {
      if (options.logger) { options.logger.info('log watcher: starts a loop') }
      /* Checks if the log watcher must stop */
      if (processWatcher.stop) {
        if (options.logger) { options.logger.info('log watcher: stop wanted') }
        loopStop = true
        return resolve()
      } else {
        /* Creates synchronously the first promise */
        new Promise(function (resolve, reject) {
          /* Calls the one-loop-process promise */
          processWatcher.runProcess(analyzers, options)
          .then(function () {
            if (options.logger) { options.logger.info('log watcher: a loop ends') }
            /* Checks if the log watcher must stop */
            if (processWatcher.stop) {
              if (options.logger) { options.logger.info('log watcher: stop wanted') }
              loopStop = true
              return resolve()
            } else {
              /* Waits the delay then loops */
              setTimeout(function () { return resolve() }, options.delay * 1000)
            }
          })
          .catch(function (err) {
            /* Depending on error value */
            switch(err) {
              /* The trigger-stop-all-analyzers event is fired */
              case processAnalyzer.triggerStopAllAnalyzers:
                if (options.logger) { options.logger.info('log watcher: all-analyzers-stop fired -> loops') }
                /* Goes to the next loop */
                return resolve()
                break
              /* The trigger-stop-watcher event is fired */
              case processAnalyzer.triggerStopWatcher:
                if (options.logger) { options.logger.info('log watcher: stop fired') }
                /* Stops the loop and stops the log watcher */
                loopStop = true
                processWatcher.stop = true
                return resolve()
                break
              /* Others errors */
              default:
                return reject(err)
            }
          })
        }) /* End of the in-loop synchronous promise */
        .then(function () {
          /* Checks if we must start a new loop */
          if (!loopStop) { loop(loopStop) }
          else {
            /* The log watcher ends */
            return resolve()
          }
        })
        .catch(function (err) { return reject(err) })
      }
    } /* End of the loop() function */
    /* Calls immediatly the looping function */
    loop(loopStop)
  })
}

/* Runs one loop process */
processWatcher.runProcess = function (analyzers, options) {
  return new Promise(function (resolve, reject) {
    /* Checks the analyzers running mode */
    if (options.sequential) {
      processWatcher.runProcessSequential(analyzers, options)
      .then(function () { return resolve() })
      .catch(function (err) { return reject(err) })
    } else {
      return processWatcher.runProcessParallel(analyzers, options)
      .then(function () { return resolve() })
      .catch(function (err) { return reject(err) })
    }
  })
}

/* Runs the analyzers in parallel mode */
processWatcher.runProcessParallel = function (analyzers, options) {
  return new Promise(function (resolve, reject) {
    /* Runs the promises in parallel mode */
    var promises = []
    for (var i = 0; i < analyzers.length; i++) {
      promises.push(processAnalyzer.run(processWatcher, analyzers[i], options))
    }
    promise.all(promises)
    .then(function () { return resolve() })
    .catch(function (err) {
      // TODO error test to loop
      return reject(err)
    })
  })
}

/* Runs the analyzers in sequence */
processWatcher.runProcessSequential = function (analyzers, options) {
  return new Promise(function (resolve, reject) {
    /* Defines the async function.
       The run() promise must be called sequentially -> await */
    async function doRun () {
      /* Loops sequentially on the analyzers */
      for (var i = 0; i < analyzers.length; i++) {
        /* Runs one analyzer */
        try {
          await processAnalyzer.run(processWatcher, analyzers[i], options)
        } catch (err) {
          // TODO error test to loop
          return reject(err)
        }
      }
      /* All the analyzers are done */
      return resolve()
    }
    /* Runs the async function */
    doRun()
  })
}

/* Exports this plugin */
module.exports = processWatcher
