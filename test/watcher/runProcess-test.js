/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const config = require('../../config/easy-log-watcher')
const path = require('path')
const processParser = require('../../process/parser')
const processWatcher = require('../../process/watcher')

/* Test prerequisites */
const Code = require('code')
const Lab = require('lab')
const lab = (exports.lab = Lab.script())
const after = lab.after
const describe = lab.describe
const it = lab.it
const expect = Code.expect

/* Backups functions before mock */
const backups = new Map()
backups.set(processWatcher, [
  ['runProcessSequential', processWatcher.runProcessSequential.bind({})],
  ['runProcessParallel', processWatcher.runProcessParallel.bind({})]
])

describe('watcher runProcess', { timeout: 5 * 1000 }, function () {
  after((done) => {
    /* Restores the origin functions */
    for (var [key, values] of backups) {
      values.forEach(function (item) { key[item[0]] = item[1] })
    }
    done()
  })
  /* Parallel */
  it('parallel error', function (fin) {
    /* Mocks */
    var msg = 'Ooops!'
    mockParallelError(msg)
    /* Fires the test */
    processWatcher.runProcess([], getOptionsParallel())
    .catch(function (err) {
      /* Checks the result */
      expect(err.message).to.equal(msg)
      fin()
    })
  })
  it('parallel run', function (fin) {
    /* Initializes */
    processWatcher.count = 0
    /* Mocks */
    mockParallelRun(processWatcher)
    /* Fires the test */
    processWatcher.runProcess([], getOptionsParallel())
    .then(function () {
      /* Checks the result */
      expect(processWatcher.count).to.equal(1)
      fin()
    })
  })
  /* Sequential */
  it('sequential error', function (fin) {
    /* Mocks */
    var msg = 'Ooops!'
    mockSequentialError(msg)
    /* Fires the test */
    processWatcher.runProcess([], getOptionsSequential())
    .catch(function (err) {
      /* Checks the result */
      expect(err.message).to.equal(msg)
      fin()
    })
  })
  it('sequential run', function (fin) {
    /* Initializes */
    processWatcher.count = 0
    /* Mocks */
    mockSequentialRun(processWatcher)
    /* Fires the test */
    processWatcher.runProcess([], getOptionsSequential())
    .then(function () {
      /* Checks the result */
      expect(processWatcher.count).to.equal(1)
      fin()
    })
  })
})

/* ---------- MOCKS ---------- */

function mockParallelError (msg) {
  processWatcher.runProcessParallel = function (analyzers, options) {
    return new Promise(function (resolve, reject) {
      return reject(new Error(msg))
    })
  }
  return false
}

function mockParallelRun (processWatcher) {
  processWatcher.runProcessParallel = function (analyzers, options) {
    return new Promise(function (resolve, reject) {
      processWatcher.count ++
      return resolve()
    })
  }
  return false
}

function mockSequentialError (msg) {
  processWatcher.runProcessSequential = function (analyzers, options) {
    return new Promise(function (resolve, reject) {
      return reject(new Error(msg))
    })
  }
  return false
}

function mockSequentialRun (processWatcher) {
  processWatcher.runProcessSequential = function (analyzers, options) {
    return new Promise(function (resolve, reject) {
      processWatcher.count ++
      return resolve()
    })
  }
  return false
}

/* ---------- FUNCTIONS ---------- */

function getOptionsParallel () {
  return {
    delay: 1,
    logger: null,
    sequential: false
  }
}

function getOptionsSequential () {
  return {
    delay: 1,
    logger: null,
    sequential: true
  }
}
