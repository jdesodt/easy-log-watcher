/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* default configuration file */
var config = {}

/* The time in seconds between two process loops */
config.delay = 3600 // 1 hour
/* The logger for the info messages */
config.logger = console
/* Analyzers run mode */
config.sequential = false

/* Exports this configuration */
module.exports = config
