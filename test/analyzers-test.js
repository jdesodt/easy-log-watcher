/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const config = require('../config/easy-log-watcher')
const logWatcher = require('../easy-log-watcher')
const path = require('path')

/* Test prerequisites */
const Code = require('code')
const Lab = require('lab')
const lab = (exports.lab = Lab.script())
const describe = lab.describe
const it = lab.it
const expect = Code.expect

describe('analyzers', { timeout: 10 * 1000 }, function () {
  /* Bad arguments */
  it('no analyzer', function (fin) {
    /* Fires the test */
    logWatcher.start()
    .then(function (result) {
      /* Checks the result */
      expect(result.success).to.equal(true)
      fin()
    })
  })
  /* OK */
  it('ok', function (fin) {
    /* Fires the test */
    logWatcher.start(getAnalyzers(), getOptions())
    .then(function (result) {
      /* Checks the result */
      expect(result.success).to.equal(true)
      fin()
    })
  })
})

/* ---------- FUNCTIONS ---------- */

function getAnalyzers () {
  return [{
    filename: path.join(__dirname, 'watcher-log.txt'),
    parsers: [{
      checker: function (line, values) { return line.indexOf('error') > -1 },
      trigger: function (file, line, values) { return 5 } // stops the log watcher
    }]
  }]
}

function getOptions () {
  return {
    delay: 0.5,
    logger: null,
    sequential: false
  }
}
