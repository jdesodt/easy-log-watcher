/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const config = require('./config/easy-log-watcher')
const processWatcher = require('./process/watcher')

/* Initializes */
var logWatcher = {}

logWatcher.start = function (analyzers, options) {
  return new Promise(function (resolve, reject) {
    /* Checks the analyzers */
    if (!analyzers || analyzers.length < 1) {
      /* No analyzer: nothing to do  */
      return resolve({ success: true })
    } else {
      /* Merge the options with the default values */
      var logWatcherOptions = options ? Object.assign(config, options) : config
      /* Runs the looping process */
      if (logWatcherOptions.logger) { logWatcherOptions.logger.info('log watcher: start with options', logWatcherOptions) }
      processWatcher.execute(analyzers, logWatcherOptions)
      .then(function () {
        if (logWatcherOptions.logger) { logWatcherOptions.logger.info('log watcher: end') }
        return resolve({ success: true })
      })
      .catch(function (err) {
        if (logWatcherOptions.logger) { logWatcherOptions.logger.info('log watcher: error', err) }
        return reject(err)
      })
    }
  })
}

logWatcher.stop = function () {
  processWatcher.stop = true
  return false
}

/* Exports this plugin */
module.exports = logWatcher
