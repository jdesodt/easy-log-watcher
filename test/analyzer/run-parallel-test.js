/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const config = require('../../config/easy-log-watcher')
const path = require('path')
const processAnalyzer = require('../../process/analyzer')
const processWatcher = require('../../process/watcher')

/* Test prerequisites */
const Code = require('code')
const Lab = require('lab')
const lab = (exports.lab = Lab.script())
const after = lab.after
const describe = lab.describe
const it = lab.it
const expect = Code.expect

/* Backups functions before mock */
const backups = new Map()
backups.set(processAnalyzer, [
  ['readFile', processAnalyzer.readFile.bind({})]
])

describe('analyzer runParallel', { timeout: 5 * 1000 }, function () {
  after((done) => {
    /* Restores the origin functions */
    for (var [key, values] of backups) {
      values.forEach(function (item) { key[item[0]] = item[1] })
    }
    done()
  })
  /* Error */
  it('error', function (fin) {
    /* Initializes */
    var message = 'Ooops!'
    /* Mocks */
    mockReadFileError(message)
    /* Fires the test */
    processAnalyzer.runParallel(processWatcher, getAnalyzer(), getFiles(), getOptions())
    .catch(function (err) {
      /* Checks the result */
      expect(err.message).to.equal(message)
      fin()
    })
  })
  /* OK */
  it('OK', function (fin) {
    /* Initializes */
    processWatcher.count = 0
    /* Mocks */
    mockReadFileOk(processWatcher)
    /* Fires the test */
    processAnalyzer.runParallel(processWatcher, getAnalyzer(), getFiles(), getOptions())
    .then(function () {
      /* Checks the result */
      expect(processWatcher.count).to.equal(getFiles().length)
      fin()
    })
  })
})

/* ---------- MOCKS ---------- */

function mockReadFileError (msg) {
  processAnalyzer.readFile = function (processWatcher, analyzer, file, options) {
    return new Promise(function (resolve, reject) {
      return reject(new Error(msg))
    })
  }
  return false
}

function mockReadFileOk (processWatcher) {
  processAnalyzer.readFile = function (processWatcher, analyzer, file, options) {
    return new Promise(function (resolve, reject) {
      processWatcher.count ++
      return resolve()
    })
  }
  return false
}

/* ---------- FUNCTIONS ---------- */

function getAnalyzer () {
  return {
    filename: path.join(__dirname, 'analyzer-log.txt'),
    parsers: [{
      checker: function (line, values) { return false },
      trigger: function (file, line, values) { return 0 } // loops indefinitely
    }],
    options: {
      sequentialFiles: false,
      sequentialParsers: false
    }
  }
}

function getFiles () {
  return [ 'analyzer-log.txt', 'analyzer-log-2.txt' ]
}

function getOptions () {
  return {
    delay: 1,
    logger: null,
    sequential: false
  }
}
