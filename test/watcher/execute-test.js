/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const config = require('../../config/easy-log-watcher')
const path = require('path')
const processAnalyzer = require('../../process/analyzer')
const processWatcher = require('../../process/watcher')

/* Test prerequisites */
const Code = require('code')
const Lab = require('lab')
const lab = (exports.lab = Lab.script())
const after = lab.after
const describe = lab.describe
const it = lab.it
const expect = Code.expect

/* Backups functions before mock */
const backups = new Map()
backups.set(processWatcher, [
  ['runProcess', processWatcher.runProcess.bind({})]
])

describe('watcher execute', { timeout: 5 * 1000 }, function () {
  after((done) => {
    /* Restores the origin functions */
    for (var [key, values] of backups) {
      values.forEach(function (item) { key[item[0]] = item[1] })
    }
    done()
  })
  /* Errors */
  it('error unknown', function (fin) {
    /* Mocks */
    var msg = 'Ooops!'
    mockRunError(msg)
    /* Fires the test */
    processWatcher.execute([], getOptions())
    .catch(function (err) {
      /* Checks the result */
      expect(err.message).to.equal(msg)
      fin()
    })
  })
  it('error trigger stop log watcher', function (fin) {
    /* Mocks */
    mockRunTriggerStopLogWatcher()
    /* Fires the test */
    processWatcher.execute([], getOptions())
    .then(function () {
      /* Checks the result */
      fin()
    })
  })
  /* Executes */
  it('loops', function (fin) {
    /* Mocks */
    mockRunLoop()
    /* Fires the test */
    processWatcher.execute([], getOptions())
    .then(function () {
      /* Checks the result */
      fin()
    })
    /* Waits some delay before stops the log watcher */
    setTimeout(function () {
      processWatcher.stop = true
    }, 1000)
  })
})

/* ---------- MOCKS ---------- */

function mockRunError (msg) {
  processWatcher.runProcess = function (analyzers, options) {
    return new Promise(function (resolve, reject) {
      return reject(new Error(msg))
    })
  }
  return false
}

function mockRunTriggerStopLogWatcher (msg) {
  processWatcher.runProcess = function (analyzers, options) {
    return new Promise(function (resolve, reject) {
      return reject(processAnalyzer.triggerStopWatcher)
    })
  }
  return false
}

function mockRunLoop () {
  processWatcher.runProcess = function (analyzers, options) {
    return new Promise(function (resolve, reject) {
      return resolve()
    })
  }
  return false
}

/* ---------- FUNCTIONS ---------- */

function getOptions () {
  return {
    delay: 0.5,
    logger: null,
    sequential: false
  }
}
