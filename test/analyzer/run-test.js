/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const config = require('../../config/easy-log-watcher')
const path = require('path')
const processAnalyzer = require('../../process/analyzer')
const processWatcher = require('../../process/watcher')

/* Test prerequisites */
const Code = require('code')
const Lab = require('lab')
const lab = (exports.lab = Lab.script())
const after = lab.after
const describe = lab.describe
const it = lab.it
const expect = Code.expect

/* Backups functions before mock */
const backups = new Map()
backups.set(processAnalyzer, [
  ['runParallel', processAnalyzer.runParallel.bind({})],
  ['runSequential', processAnalyzer.runSequential.bind({})]
])

describe('analyzer run', { timeout: 5 * 1000 }, function () {
  after((done) => {
    /* Restores the origin functions */
    for (var [key, values] of backups) {
      values.forEach(function (item) { key[item[0]] = item[1] })
    }
    done()
  })
  /* Log watcher stop */
  it('log watcher stop', function (fin) {
    /* Initializes */
    processWatcher.stop = true
    /* Fires the test */
    processAnalyzer.run(processWatcher, getAnalyzer(), getOptions())
    .catch(function (err) {
      /* Checks the result */
      expect(err).to.equal(processAnalyzer.triggerStopWatcher)
      fin()
    })
  })
  /* No data */
  it('no filename', function (fin) {
    /* Initializes */
    processWatcher.stop = false
    /* Fires the test */
    processAnalyzer.run(processWatcher, {}, getOptions())
    .then(function () {
      /* Checks the result */
      fin()
    })
  })
  it('no parsers array', function (fin) {
    /* Fires the test */
    processAnalyzer.run(processWatcher, { filename: 'foo' }, getOptions())
    .then(function () {
      /* Checks the result */
      fin()
    })
  })
  it('parsers array empty', function (fin) {
    /* Fires the test */
    processAnalyzer.run(processWatcher, { filename: 'foo', parsers: [] }, getOptions())
    .then(function () {
      /* Checks the result */
      fin()
    })
  })
  it('no log file', function (fin) {
    /* Initializes */
    var analyzer = getAnalyzer()
    analyzer.filename = 'nofile.txt'
    /* Fires the test */
    processAnalyzer.run(processWatcher, { filename: 'foo', parsers: [] }, getOptions())
    .then(function () {
      /* Checks the result */
      fin()
    })
  })
  /* Parallel */
  it('parallel error', function (fin) {
    /* Initializes */
    var message = 'Ooops!'
    /* Mocks */
    mockParallelError(message)
    /* Fires the test */
    processAnalyzer.run(processWatcher, getAnalyzerParallel(), getOptions())
    .catch(function (err) {
      /* Checks the result */
      expect(err.message).to.equal(message)
      fin()
    })
  })
  it('parallel OK', function (fin) {
    /* Mocks */
    mockParallelOk(processWatcher)
    /* Fires the test */
    processAnalyzer.run(processWatcher, getAnalyzerParallel(), getOptions())
    .then(function () {
      /* Checks the result */
      expect(processWatcher.count).to.equal(1)
      fin()
    })
  })
  /* Sequential */
  it('sequential error', function (fin) {
    /* Initializes */
    var message = 'Ooops!'
    /* Mocks */
    mockSequentialError(message)
    /* Fires the test */
    processAnalyzer.run(processWatcher, getAnalyzerSequential(), getOptions())
    .catch(function (err) {
      /* Checks the result */
      expect(err.message).to.equal(message)
      fin()
    })
  })
  it('sequential OK', function (fin) {
    /* Mocks */
    mockSequentialOk(processWatcher)
    /* Fires the test */
    processAnalyzer.run(processWatcher, getAnalyzerSequential(), getOptions())
    .then(function () {
      /* Checks the result */
      expect(processWatcher.count).to.equal(1)
      fin()
    })
  })
})

/* ---------- MOCKS ---------- */

function mockParallelError (msg) {
  processAnalyzer.runParallel = function (processWatcher, analyzer, files, options) {
    return new Promise(function (resolve, reject) {
      return reject(new Error(msg))
    })
  }
  return false
}

function mockParallelOk (processWatcher) {
  processWatcher.count = 0
  processAnalyzer.runParallel = function (processWatcher, analyzer, files, options) {
    return new Promise(function (resolve, reject) {
      processWatcher.count ++
      return resolve()
    })
  }
  return false
}

function mockSequentialError (msg) {
  processAnalyzer.runSequential = function (processWatcher, analyzer, files, options) {
    return new Promise(function (resolve, reject) {
      return reject(new Error(msg))
    })
  }
  return false
}

function mockSequentialOk (processWatcher) {
  processWatcher.count = 0
  processAnalyzer.runSequential = function (processWatcher, analyzer, files, options) {
    return new Promise(function (resolve, reject) {
      processWatcher.count ++
      return resolve()
    })
  }
  return false
}

/* ---------- FUNCTIONS ---------- */

function getAnalyzer () {
  return {
    filename: path.join(__dirname, 'analyzer-log.txt'),
    parsers: [{
      checker: function (line, values) { return false },
      trigger: function (file, line, values) { return 0 } // loops indefinitely
    }],
    options: {
      sequentialFiles: false,
      sequentialParsers: false
    }
  }
}

function getAnalyzerParallel () {
  var analyzer = getAnalyzer()
  analyzer.options.sequentialFiles = false
  return analyzer
}

function getAnalyzerSequential () {
  var analyzer = getAnalyzer()
  analyzer.options.sequentialFiles = true
  return analyzer
}

function getOptions () {
  return {
    delay: 1,
    logger: null,
    sequential: false
  }
}
