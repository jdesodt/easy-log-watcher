/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const promise = require('bluebird')

/* Special subclasses of Error to catch the trigger stop event  */
class ProcessParserTriggerStopParser extends Error {
  constructor(message) {
    super(message)
    this.name = this.constructor.name
  }
}
class ProcessParserTriggerStopAllParsers extends Error {
  constructor(message) {
    super(message)
    this.name = this.constructor.name
  }
}

/* Initializes */
var processParser = {}
processParser.triggerStopParser = new ProcessParserTriggerStopParser()
processParser.triggerStopAllParsers = new ProcessParserTriggerStopAllParsers()


/* Runs one parser */
processParser.parse = function (processWatcher, processAnalyzer, parser, file, line, options) {
  return new Promise(function (resolve, reject) {
    /* Checks if the log watcher must stop */
    if (processWatcher.stop) {
      if (options.logger) { options.logger.info('log watcher: stop wanted') }
      return reject(processAnalyzer.triggerStopWatcher)
    }
    else {
      /* Checks if the line must fire the trigger */
      if (line && parser.checker(line, parser.values)) {
        if (options.logger) { options.logger.info('log watcher: file', file, ', checked line:', line.substring(0, Math.min(line.length, 100))) }
        /* Starts the trigger */
        if (options.logger) { options.logger.info('log watcher: the trigger starts', parser.trigger) }
        var exitCode = parser.trigger(file, line, parser.values)
        /* Depending on the exit code */
        switch(exitCode) {
          /* No stop: next line */
          case 0:
            return resolve()
            break
          /* Stops this parser */
          case 1:
            return reject(processParser.triggerStopParser)
            break
          /* Stops all the parsers */
          case 2:
            return reject(processParser.triggerStopAllParsers)
            break
          /* Stops the analyzer */
          case 3:
            return reject(processAnalyzer.triggerStopAnalyzer)
            break
          /* Stops all the analyzers */
          case 4:
            return reject(processAnalyzer.triggerStopAllAnalyzers)
            break
          /* Stops all the analyzers */
          case 5:
            return reject(processAnalyzer.triggerStopWatcher)
            break
          /* Others cases */
          default:
            return resolve()
        }
      /* The line is not checked */
      } else { return resolve() }
    }
  })
}

/* Runs the parsers in parallel mode */
processParser.runParallel = function (processWatcher, processAnalyzer, parsers, file, line, options) {
  return new Promise(function (resolve, reject) {
    /* Runs the promises in parallel mode */
    var promises = []
    for (var i = 0; i < parsers.length; i++) {
      promises.push(processParser.parse(processWatcher, processAnalyzer, parsers[i], file, line, options))
    }
    promise.all(promises)
    .then(function () { return resolve() })
    .catch(function (err) {
      /* Checks the error */
      switch(err) {
        /* The trigger-stop-parser event is fired */
        case processParser.triggerStopParser:
          if (options.logger) { options.logger.info('log watcher: parser-stop fired in parallel mode -> next file') }
          /* Goes to the next log file */
          return resolve()
          break
        /* The trigger-stop-all-parsers event is fired */
        case processParser.triggerStopAllParsers:
          if (options.logger) { options.logger.info('log watcher: all-parsers-stop fired -> next file') }
          /* Goes to the next log file */
          return resolve()
          break
        /* Others errors */
        default:
          return reject(err)
      }
    })
  })
}

/* Runs the parsers sequentially */
processParser.runSequential = function (processWatcher, processAnalyzer, parsers, file, line, options) {
  return new Promise(function (resolve, reject) {
    /* Defines the async function.
       The parse() promise must be called sequentially -> await */
    async function doParse () {
      /* Loops sequentially on the parsers */
      for (var i = 0; i < parsers.length; i++) {
        /* Checks if the log watcher must stop */
        if (processWatcher.stop) {
          if (options.logger) { options.logger.info('log watcher: stop wanted') }
          return reject(processAnalyzer.triggerStopWatcher)
        }
        else {
          /* Runs one parser */
          if (options.logger) { options.logger.info('log watcher: run parser', parsers[i].checker) }
          try {
            await processParser.parse(processWatcher, processAnalyzer, parsers[i], file, line, options)
          } catch (err) {
            /* Checks the error */
            switch(err) {
              /* The trigger-stop-parser event is fired */
              case processParser.triggerStopParser:
                if (options.logger) { options.logger.info('log watcher: parser-stop fired -> next parser') }
                /* Goes to the next parser in the for() loop */
                break
              /* The trigger-stop-all-parsers event is fired */
              case processParser.triggerStopAllParsers:
                if (options.logger) { options.logger.info('log watcher: all-parsers-stop fired -> next file') }
                /* Goes to the next log file */
                return resolve()
                break
              /* Others errors */
              default:
                return reject(err)
            }
          }
        }
      }
      /* All the parsers have been run */
      return resolve()
    }
    /* Runs the async function */
    doParse()
  })
}

/* Exports this plugin */
module.exports = processParser
