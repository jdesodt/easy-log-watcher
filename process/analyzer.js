/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const config = require('../config/analyzer')
const fg = require('fast-glob')
const lineByLine = require('line-by-line')
const processParser = require('./parser')
const promise = require('bluebird')

/* Special subclasses of Error to catch the trigger stop events  */
class ProcessAnalyzerStopAnalyzer extends Error {
  constructor(message) {
    super(message)
    this.name = this.constructor.name
  }
}
class ProcessAnalyzerStopAllAnalyzers extends Error {
  constructor(message) {
    super(message)
    this.name = this.constructor.name
  }
}
class ProcessAnalyzerStopWatcher extends Error {
  constructor(message) {
    super(message)
    this.name = this.constructor.name
  }
}

/* Initializes */
var processAnalyzer = {}
processAnalyzer.triggerStopAnalyzer = new ProcessAnalyzerStopAnalyzer()
processAnalyzer.triggerStopAllAnalyzers = new ProcessAnalyzerStopAllAnalyzers()
processAnalyzer.triggerStopWatcher = new ProcessAnalyzerStopWatcher()

/* Closes the file reader and rejects the error */
processAnalyzer.closeReaderOnError = function (reader, err, resolve, reject, options) {
  /* Desacivates the reader end event function */
  reader.on('end', function () { /* Do nothing */ })
  /* Closes the reader */
  reader.close()
  /* Returns the error */
  return reject(err)
}

/* Reads one file */
processAnalyzer.readFile = function (processWatcher, analyzer, file, options) {
  return new Promise(function (resolve, reject) {
    /* Initializes */
    if (options.logger) { options.logger.info('log watcher: read file', file) }
    var reader = new lineByLine(file)
    /* WARNING: the end event must be THE FIRST managed events because
       it is overriden when an error occurs.
       The file is fully read */
    reader.on('end', function () { return resolve() })
    /* Manages reader error */
    reader.on('error', function (err) {
      /* Checks the 'no such file' error./
         The 'no such file' error can be fired if the file is deleted during the process,
         or if the file does not exist. Anyway the process must continue.
         So this is not an error. */
      if (err.message.indexOf('no such file') > -1) { return resolve() }
      else { return reject(err) }
    })
    /* Reads line by line */
    reader.on('line', function (line) {
      /* Pauses emitting of lines */
      reader.pause()
      /* Checks if the log watcher must be stopped */
      if (processWatcher.stop) {
        if (options.logger) { options.logger.info('log watcher: stop wanted') }
        /* Closes the reader and manages the error */
        var stopError = processAnalyzer.triggerStopWatcher
        return processAnalyzer.closeReaderOnError(reader, stopError, resolve, reject, options)
      } else {
        /* Process the line */
        if (options.logger) { options.logger.info('log watcher: read file', file, 'line', line.substring(0, Math.min(line.length, 100))) }
        /* Checks the parsers mode */
        if (analyzer.options.sequentialParsers) {
          processParser.runSequential(processWatcher, processAnalyzer, analyzer.parsers, file, line, options)
          .then(function () { /* Nothing to do -> next line */ })
          .catch(function (err) {
            /* Closes the reader and rejects the error */
            return processAnalyzer.closeReaderOnError(reader, err, resolve, reject, options)
          })
        } else {
          processParser.runParallel(processWatcher, processAnalyzer, analyzer.parsers, file, line, options)
          .then(function () { /* Nothing to do -> next line */ })
          .catch(function (err) {
            /* Closes the reader and rejects the error */
            return processAnalyzer.closeReaderOnError(reader, err, resolve, reject, options)
          })
        }
        /* Coninues emitting lines */
        setTimeout(function () { reader.resume() }, 100)
      }
    })
  })
}

/* Runs the analyzer */
processAnalyzer.run = function (processWatcher, analyzer, options) {
  return new Promise(function (resolve, reject) {
    /* Checks if the log watcher must stop */
    if (processWatcher.stop) {
      if (options.logger) { options.logger.info('log watcher: stop wanted') }
      return reject(processAnalyzer.triggerStopWatcher)
    } else {
      /* Checks if there is a filename and some parsers */
      if (!analyzer.filename || !analyzer.parsers || analyzer.parsers.length < 1) {
        return resolve()
      } else {
        /* Merges the options with the default values */
        analyzer.options = analyzer.options ? Object.assign(config, analyzer.options) : config
        /* Gets the files to process */
        fg.async([analyzer.filename])
        .then(function (files) {
          /* Checks if there are some files to process */
          if (files.length > 0) {
            /* Checks the read files mode */
            if (analyzer.options.sequentialFiles) {
              processAnalyzer.runSequential(processWatcher, analyzer, files, options)
              .then(function () { return resolve() })
              .catch(function (err) { return reject(err) })
            } else {
              processAnalyzer.runParallel(processWatcher, analyzer, files, options)
              .then(function () { return resolve() })
              .catch(function (err) { return reject(err) })
            }
          /* No file to process: returns */
          } else { return resolve() }
        })
        .catch(function (err) { return reject(err) })
      }
    }
  })
}

/* Reads the files in parallel mode */
processAnalyzer.runParallel = function (processWatcher, analyzer, files, options) {
  return new Promise(function (resolve, reject) {
    /* Runs the promises in parallel mode */
    var promises = []
    for (var i = 0; i < files.length; i++) {
      promises.push(processAnalyzer.readFile(processWatcher, analyzer, files[i], options))
    }
    promise.all(promises)
    .then(function () { return resolve() })
    .catch(function (err) { return reject(err) })
  })
}

/* Reads the files sequentially */
processAnalyzer.runSequential = function (processWatcher, analyzer, files, options) {
  return new Promise(function (resolve, reject) {
    /* Defines the async function.
       The readFile() promise must be called sequentially -> await */
    async function read () {
      /* Loops sequentially on the files */
      for (var i = 0; i < files.length; i++) {
        /* Checks if the log watcher must stop */
        if (processWatcher.stop) {
          if (options.logger) { options.logger.info('log watcher: stop wanted') }
          return reject(processAnalyzer.triggerStopWatcher)
        }
        else {
          /* Reads one file */
          try {
            await processAnalyzer.readFile(processWatcher, analyzer, files[i], options)
          } catch (err) { return reject(err) }
        }
      }
      /* All the files are read */
      return resolve()
    }
    /* Runs the async function */
    read()
  })
}

/* Exports this plugin */
module.exports = processAnalyzer
