/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const config = require('../../config/easy-log-watcher')
const path = require('path')
const processAnalyzer = require('../../process/analyzer')
const processParser = require('../../process/parser')
const processWatcher = require('../../process/watcher')

/* Test prerequisites */
const Code = require('code')
const Lab = require('lab')
const lab = (exports.lab = Lab.script())
const describe = lab.describe
const it = lab.it
const expect = Code.expect

describe('parser parse', { timeout: 5 * 1000 }, function () {
  /* Log watcher stop */
  it('log watcher stop', function (fin) {
    /* Initializes */
    processWatcher.stop = true
    /* Fires the test */
    processParser.parse(processWatcher, processAnalyzer, null, null, null, getOptions())
    .catch(function (err) {
      /* Checks the result */
      expect(err).equal(processAnalyzer.triggerStopWatcher)
      fin()
    })
  })
  /* No line */
  it('no line', function (fin) {
    /* Initializes */
    processWatcher.stop = false
    /* Fires the test */
    processParser.parse(processWatcher, processAnalyzer, null, null, null, getOptions())
    .then(function () {
      /* Checks the result */
      fin()
    })
  })
  it('empty line', function (fin) {
    /* Fires the test */
    processParser.parse(processWatcher, processAnalyzer, null, null, '', getOptions())
    .then(function () {
      /* Checks the result */
      fin()
    })
  })
  /* No check */
  it('no check', function (fin) {
    /* Fires the test */
    processParser.parse(processWatcher, processAnalyzer, getParserNoCheck(), null, 'a line', getOptions())
    .then(function () {
      /* Checks the result */
      fin()
    })
  })
  /* Check */
  it('check and return 0', function (fin) {
    /* Initializes */
    processWatcher.count = 0
    /* Fires the test */
    processParser.parse(processWatcher, processAnalyzer, getParser(processWatcher, 0), null, 'a line', getOptions())
    .then(function () {
      /* Checks the result */
      expect(processWatcher.count).to.equal(1)
      fin()
    })
  })
  it('check and return 1', function (fin) {
    /* Initializes */
    processWatcher.count = 0
    /* Fires the test */
    processParser.parse(processWatcher, processAnalyzer, getParser(processWatcher, 1), null, 'a line', getOptions())
    .catch(function (err) {
      /* Checks the result */
      expect(err).equal(processParser.triggerStopParser)
      fin()
    })
  })
  it('check and return 2', function (fin) {
    /* Initializes */
    processWatcher.count = 0
    /* Fires the test */
    processParser.parse(processWatcher, processAnalyzer, getParser(processWatcher, 2), null, 'a line', getOptions())
    .catch(function (err) {
      /* Checks the result */
      expect(err).equal(processParser.triggerStopAllParsers)
      fin()
    })
  })
  it('check and return 3', function (fin) {
    /* Initializes */
    processWatcher.count = 0
    /* Fires the test */
    processParser.parse(processWatcher, processAnalyzer, getParser(processWatcher, 3), null, 'a line', getOptions())
    .catch(function (err) {
      /* Checks the result */
      expect(err).equal(processAnalyzer.triggerStopAnalyzer)
      fin()
    })
  })
  it('check and return 4', function (fin) {
    /* Initializes */
    processWatcher.count = 0
    /* Fires the test */
    processParser.parse(processWatcher, processAnalyzer, getParser(processWatcher, 4), null, 'a line', getOptions())
    .catch(function (err) {
      /* Checks the result */
      expect(err).equal(processAnalyzer.triggerStopAllAnalyzers)
      fin()
    })
  })
  it('check and return 5', function (fin) {
    /* Initializes */
    processWatcher.count = 0
    /* Fires the test */
    processParser.parse(processWatcher, processAnalyzer, getParser(processWatcher, 5), null, 'a line', getOptions())
    .catch(function (err) {
      /* Checks the result */
      expect(err).equal(processAnalyzer.triggerStopWatcher)
      fin()
    })
  })
  it('check and return other value', function (fin) {
    /* Initializes */
    processWatcher.count = 0
    /* Fires the test */
    processParser.parse(processWatcher, processAnalyzer, getParser(processWatcher, 15), null, 'a line', getOptions())
    .then(function () {
      /* Checks the result */
      expect(processWatcher.count).to.equal(1)
      fin()
    })
  })
})

/* ---------- FUNCTIONS ---------- */

function getOptions () {
  return {
    delay: 1,
    logger: null,
    sequential: false
  }
}

function getParser (processWatcher, exitCode) {
  return {
    checker: function (line, values) { return true },
    trigger: function (file, line, values) {
      values.processWatcher.count ++
      return values.exitCode
    },
    values: {
      processWatcher: processWatcher,
      exitCode: exitCode
    }
  }
}

function getParserNoCheck () {
  return {
    checker: function (line, values) { return false },
    trigger: function (file, line, values) { return 0 }
  }
}
