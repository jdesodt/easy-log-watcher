/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const config = require('../../config/easy-log-watcher')
const path = require('path')
const processAnalyzer = require('../../process/analyzer')
const processParser = require('../../process/parser')
const processWatcher = require('../../process/watcher')

/* Test prerequisites */
const Code = require('code')
const Lab = require('lab')
const lab = (exports.lab = Lab.script())
const after = lab.after
const describe = lab.describe
const it = lab.it
const expect = Code.expect

/* Backups functions before mock */
const backups = new Map()
backups.set(processParser, [
  ['parse', processParser.parse.bind({})]
])

describe('parser runSequential', { timeout: 5 * 1000 }, function () {
  after((done) => {
    /* Restores the origin functions */
    for (var [key, values] of backups) {
      values.forEach(function (item) { key[item[0]] = item[1] })
    }
    done()
  })
  /* Error */
  it('error', function (fin) {
    /* Initializes */
    var message = 'Ooops!'
    /* Mocks */
    mockParseError(message)
    /* Fires the test */
    processParser.runSequential(processWatcher, processAnalyzer, getParsers(), getFile(), 'a line', getOptions())
    .catch(function (err) {
      /* Checks the result */
      expect(err.message).to.equal(message)
      fin()
    })
  })
  it('parser stop', function (fin) {
    /* Initializes */
    processWatcher.count = 0
    /* Mocks */
    mockParseErrorObject(processWatcher, processParser.triggerStopParser)
    /* Fires the test */
    processParser.runSequential(processWatcher, processAnalyzer, getParsers(), getFile(), 'a line', getOptions())
    .then(function () {
      /* Checks the result */
      expect(processWatcher.count).to.equal(getParsers().length)
      fin()
    })
  })
  it('all parsers stop', function (fin) {
    /* Initializes */
    processWatcher.count = 0
    /* Mocks */
    mockParseErrorObject(processWatcher, processParser.triggerStopAllParsers)
    /* Fires the test */
    processParser.runSequential(processWatcher, processAnalyzer, getParsers(), getFile(), 'a line', getOptions())
    .then(function () {
      /* Checks the result */
      expect(processWatcher.count).to.equal(1)
      fin()
    })
  })
  /* OK */
  it('OK', function (fin) {
    /* Initializes */
    processWatcher.count = 0
    /* Mocks */
    mockParserOk(processWatcher)
    /* Fires the test */
    processParser.runSequential(processWatcher, processAnalyzer, getParsers(), getFile(), 'a line', getOptions())
    .then(function () {
      /* Checks the result */
      expect(processWatcher.count).to.equal(getParsers().length)
      fin()
    })
  })
})

/* ---------- MOCKS ---------- */

function mockParseError (msg) {
  processParser.parse = function (processWatcher, processAnalyzer, parser, file, line, options) {
    return new Promise(function (resolve, reject) {
      return reject(new Error(msg))
    })
  }
  return false
}

function mockParseErrorObject (processWatcher, error) {
  processParser.parse = function (processWatcher, processAnalyzer, parser, file, line, options) {
    return new Promise(function (resolve, reject) {
      processWatcher.count ++
      return reject(error)
    })
  }
  return false
}

function mockParserOk (processWatcher) {
  processParser.parse = function (processWatcher, processAnalyzer, parser, file, line, options) {
    return new Promise(function (resolve, reject) {
      processWatcher.count ++
      return resolve()
    })
  }
  return false
}

/* ---------- FUNCTIONS ---------- */

function getFile () {
  return 'read-file-log.txt'
}

function getOptions () {
  return {
    delay: 1,
    logger: null,
    sequential: false
  }
}

function getParsers () {
  return [
    {
      checker: function (line, values) { return false },
      trigger: function (file, line, values) { return 0 } // loops indefinitely
    },
    {
      checker: function (line, values) { return true },
      trigger: function (file, line, values) { return 0 } // loops indefinitely
    }
  ]
}
