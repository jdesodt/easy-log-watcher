/* Copyright (c) 2018 e-soa Jacques Desodt, MIT License */
'use strict'

/* Prerequisites */
const config = require('../../config/easy-log-watcher')
const lineByLine = require('line-by-line')
const path = require('path')
const processAnalyzer = require('../../process/analyzer')
const processParser = require('../../process/parser')

/* Test prerequisites */
const Code = require('code')
const Lab = require('lab')
const lab = (exports.lab = Lab.script())
const describe = lab.describe
const it = lab.it
const expect = Code.expect

describe('analyzer closeReaderOnError', { timeout: 5 * 1000 }, function () {
  it('all errors', function (fin) {
    /* Initializes */
    var reader = new lineByLine(path.join(__dirname, 'analyzer-log.txt'))
    reader._initStream()
    var err = new Error('Ooops!')
    var resolve = function () { return false }
    var reject = function (err) { return true }
    /* Fires the test */
    var result = processAnalyzer.closeReaderOnError(reader, err, resolve, reject, getOptions())
    /* Checks the result */
    expect(result).to.equal(true)
    fin()
  })
})

/* ---------- FUNCTIONS ---------- */

function getOptions () {
  return {
    delay: 1,
    logger: null,
    sequential: false
  }
}
